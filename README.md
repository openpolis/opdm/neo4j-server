## Introduction

This project shows how to configure and run a
[neo4j server](https://neo4j.com/)
on a [docker](https://www.docker.com/) stack,
using [docker-compose](https://docs.docker.com/compose/), with specific instructions
for synchronising data from the [OPDM project](https://service.opdm.openpolis.io/about/).


## Setup
Before starting, the NEO4J_PASS env variable must be set to some value:
```
export NEO4J_PASS=test
```

We'll also define a ``dc`` alias, that stands for ``docker-compose``:
```
alias dc='docker-compose'
```

To start the server and test the configuration:
```
dc up -d
```

This will pull the latest image, and start a neo4j server, listening on port 7474.
The database will be empty at this point.

`data` and `logs` are persistent on global volumes (`docker volume ls`)

## Sync opdm data

Data are created using [Openpolis' metabase](https://metabase.openpolis.io),
and exported as CSV files into a local `import` directory.

They need to be manipulated and headers should be separated from the values,
as shown in the documentation of `neo4j-admin import`.

All csv files then need to be copied in to the `import` directory of the server:
```
for file in $(ls import/*)
do
   docker cp $file opdm-neo4j_neo4j_1:/var/lib/neo4j/import
done
```

The server needs to be stopped, and the `data` directory reset to an empty state,
before the real import.
```
dc stop neo4j
dc run neo4j bash

> rm -rf data/*
> HEAP_SIZE=2G bin/neo4j-admin import --multiline-fields=true \
  --nodes="import/aree_header.csv,import/aree.csv" \
  --nodes="import/persone_header.csv,import/persone.csv" \
  --nodes="import/organizzazioni_header.csv,import/organizzazioni.csv" \
  --relationships="import/incarichi_header.csv,import/incarichi.csv" \
  --relationships="import/aree_parents_header.csv,import/aree_parents.csv" \
  --relationships="import/organizzazioni_parents_header.csv,import/organizzazioni_parents.csv" \
  --relationships="import/organizzazioni_aree_header.csv,import/organizzazioni_aree.csv"
> exit
```

The server can the be started again:
```
dc up -d
```

After about 20 seconds, a **neo4j-browser*** is available at:
http://$REMOTE_IP:7474

To authenticate, use these credentials:

```
   Username: neo4j
   Password: $NEO4J_PASS
```

## TODO

* Use a third-level domain (neo4j.opdm.openpolis.io)
* Use https on the TLD (https://neo4j.opdm.openpolis.io)
* Install neo4j plugins
* Automated or batch procedure to sync data from OPDM
